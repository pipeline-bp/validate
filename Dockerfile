# Builders
FROM hadolint/hadolint:v1.17.5-8-gc8bf307-debian as hadolint

FROM golang:1.13-alpine as builder
RUN apk --no-cache add git make gcc musl-dev zip
ENV GO111MODULE=on
RUN go get -u github.com/liamg/tfsec/cmd/tfsec \
    && go get -u github.com/terraform-linters/tflint

## Building the actual image
FROM hashicorp/terraform:0.12.24
COPY --from=hadolint /bin/hadolint /usr/local/bin
COPY --from=builder /go/bin /usr/local/bin
ENTRYPOINT [ "terraform" ]
CMD [ "version" ]